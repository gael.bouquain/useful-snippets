# Useful-snippets

This extension adds 7 snippets USEFUL in json and javascript.

## Tutorial

<!-- \!\[feature X\]\(images/feature-x.png\) -->
![Snippets exemple](https://media.giphy.com/media/H4DtWw03igHMA6ShiC/giphy.gif)


## Supported languages (file extensions)

* Javascript
* Json


## Snippets

|Prefix|Method|
|-------:|-------|
|`clg→`|`console.log(object)`|
|`array→`|`let name = [elementOneName, elementTwoName]`|
|`objr→`|`let object_name = {element_name: {element_name: "content"},}`|
|`objs→`|`let object_name = {element_name: "content",}`|
|`obje→`|`element_name: "content",`|
|`objt→`|`let test = {toto: {"age: 10, parent_number: 2, soeur: 1, soeur_name: "lili", type: "enfant",}}`|
|`react-state→`|`constructor(props) {super(props) this.state = {element_name: element_content,}}`|


## Release Notes

### 0.0.1
7 Useful snippets !


**Enjoy!**